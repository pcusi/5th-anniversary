export interface PhrasesInterface {
  phrases: {
    title: string;
    description: string;
    img: string[];
    emoji: string;
  }[];
}
