import { createTheme } from "@mui/material";
import { MediaQueryEnum } from "../interfaces/media.enum";

export const Palette = {
  primary: {
    main: "#FFF8DC",
  },
};

export const theme = createTheme({
  palette: {
    primary: {
      ...Palette.primary,
    },
  },
  typography: {
    fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
    h1: {
      [MediaQueryEnum.MOBILE]: { fontSize: "48px" },
    },
    h6: {
      [MediaQueryEnum.MOBILE]: { fontSize: "16px" },
    },
  },
});
