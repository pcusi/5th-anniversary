import { MediaQueryEnum } from "../interfaces/media.enum";

export const slidersStyles = {
  card: {
    bgcolor: "primary.main",
    height: "100vh",
    boxShadow: "none",
    padding: 4,
    ".card__img": {
      width: "325px",
      height: "450px",
      display: "block",
      borderRadius: 2,
    },
    [MediaQueryEnum.MOBILE]: {
      height: "auto",
      ".card__img": {
        width: "100%",
        height: "auto",
        display: "block",
        borderRadius: 2,
      },
    },
  },
  grid: {
    height: "100%",
    [MediaQueryEnum.MOBILE]: {
      height: "auto",
    },
  },
  emoji: {
    position: "relative",
    height: "100%",
    ".emoji": {
      position: "absolute",
      bottom: 0,
      left: "50%",
      transform: "translateX(-50%)",
      fontSize: "244px",
    },
    [MediaQueryEnum.MOBILE]: {
      height: "132px",
      ".emoji": {
        height: "100%",
        fontSize: "128px",
      },
    },
  },
  swipe: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, 100%)",
  },
  video: {
    ".video": {
      width: "1189px",
      height: "669px",
      [MediaQueryEnum.MOBILE]: {
        width: "320px",
        height: "450px",
      },
    },
  },
};
