import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";

import data from "../assets/phrases.json";

import { Box, Card, Grid, Typography } from "@mui/material";
import { slidersStyles } from "./sliders.styles";
import { PhrasesInterface } from "../interfaces/phrases.interface";

export const Sliders = () => {
  const { phrases }: PhrasesInterface = JSON.parse(JSON.stringify(data));

  return (
    <Swiper>
      <SwiperSlide>
        <Typography
          variant="h1"
          color="text.dark"
          textAlign="center"
          fontWeight={600}
          sx={slidersStyles.swipe}
        >
          {"Desliza cuando termines de leer!"}
        </Typography>
      </SwiperSlide>
      {phrases.map(({ title, description, emoji, img }) => (
        <SwiperSlide key={Math.random()}>
          <Card sx={slidersStyles.card}>
            <Grid container sx={slidersStyles.grid} spacing={4}>
              <Grid lg={8} xs={12} item alignSelf="center">
                <Typography
                  variant="h1"
                  color="text.dark"
                  fontWeight={600}
                  mb={2}
                >
                  {title}
                </Typography>
                <Typography variant="h6" color="text.dark" mb={2}>
                  {description}
                </Typography>
                <Grid container spacing={2}>
                  {img.map((value, index) => (
                    <Grid item lg={4} xs={12} key={Math.random()}>
                      <img
                        src={value}
                        alt={`test-${index}`}
                        className="card__img"
                      />
                    </Grid>
                  ))}
                </Grid>
              </Grid>
              <Grid
                lg={4}
                xs={12}
                item
                alignSelf="center"
                sx={slidersStyles.emoji}
              >
                <Typography
                  variant="h1"
                  component="span"
                  color="text.dark"
                  textAlign="center"
                  className="emoji"
                >
                  {emoji}
                </Typography>
              </Grid>
            </Grid>
          </Card>
        </SwiperSlide>
      ))}
      <SwiperSlide>
        <Typography
          variant="h1"
          color="text.dark"
          textAlign="center"
          fontWeight={600}
          sx={slidersStyles.swipe}
        >
          {"Ahora, escucha estas canciones!!!!!"}
        </Typography>
      </SwiperSlide>
      <SwiperSlide>
        <Box sx={slidersStyles.video}>
          <iframe
            src="https://www.youtube.com/embed/k4V3Mo61fJM"
            title="Coldplay - Fix You (Official Video)"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
            className="video"
          ></iframe>
        </Box>
      </SwiperSlide>
      <SwiperSlide>
        <Box sx={slidersStyles.video}>
          <iframe
            src="https://www.youtube.com/embed/fV4DiAyExN0"
            title="Hoobastank - The Reason (Official Music Video)"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
            className="video"
          ></iframe>
        </Box>
      </SwiperSlide>
      <SwiperSlide>
        <Box sx={slidersStyles.video}>
          <iframe
            src="https://www.youtube.com/embed/KYZlT2iYRh8"
            title="Chayanne - Un Siglo Sin Ti (Video Oficial)"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
            className="video"
          ></iframe>
        </Box>
      </SwiperSlide>
    </Swiper>
  );
};
